FROM node:8.16.0

RUN mkdir -p /home/node/UI-app/node_modules && chown -R node:node /home/node/UI-app 

WORKDIR /home/node/UI-app

COPY package*.json ./

USER node

RUN npm install

COPY --chown=node:node . .

EXPOSE 8081 

CMD [ "npm", "start" ]
