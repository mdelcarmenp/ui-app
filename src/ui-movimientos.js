/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-card/paper-card.js';
import('./bwt-datatable-3.0.0/bwt-datatable.js');
import('./bwt-datatable-3.0.0/bwt-datatable-card.js');


import './shared-styles.js';
import {UIApp} from './ui-app.js';

class UIMovimientos extends PolymerElement {
    constructor(){
       super();
    }

    ready(){
        super.ready();
        console.log("Movimientos");
	// XXX esto tiene que poder venir del host
        this.cuentaID=localStorage.getItem("cuentaID");
        console.log(this.cuentaID);
    }

    static get properties () {
        return {
            cuentaID: {
                type: String 
            },
	    movimientos: {
		type:  Array
	    },
	    visibleahora:{
		type: String,
		observer: '_visibleahoraChanged' 
	    },
            backendapp:{
                    type: String,
                    value: "http://backend:4001"
            }

        }
    }  
  

  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>

      <ui-app>
      <div class="card">
        <h1>Movimientos de cuenta {{cuentaID}}</h1>
        <p>Aquí podrá ver los movimientos de su cuenta</p>
	<paper-datatable data=[[movimientos]]>
           <paper-datatable-column header="Fecha" property="fecha" ></paper-datatable-column>
           <paper-datatable-column header="Operación" property="operacion"></paper-datatable-column>
           <paper-datatable-column header="Concepto" property="concepto"></paper-datatable-column>
           <paper-datatable-column header="Monto" property="monto" type="Number" align="right" ></paper-datatable-column>
           <paper-datatable-column header="Subtotal" property="subtotal" type="Number" align="right" ></paper-datatable-column>
        </paper-datatable>
      </div>
      </ui-app>
    `;
  }

async     _visibleahoraChanged(page){
	    if(page && page=="movimientos"){
		    console.log('Update data ' + page);
                    this.cuentaID=localStorage.getItem("cuentaID");
	            this.movimientos = await this._traerMovimientos(this.cuentaID);
	    }
    }
    
async    _traerMovimientos(c){
	    return await fetch(this.backendapp+'/movimientos',{
		    method: 'post',
		    body: JSON.stringify({"cuentaID":c}),
		    headers:{
			    'Content-Type': 'application/json'
		    }
	          }  
	        ).then(resp => resp.json())
	        .then(data => {
			var subtotal = 0;
			console.log(data);
			for (let i = data.length - 1; i >=0 ; i--) {
			      if(data[i].fuente == c){
				      data[i].monto = -data[i].monto;
				      if(data[i].destino == ""){
					      data[i].operacion = "EXTRACCION";
				      }else{
					      data[i].operacion = "TRANSFERENCIA A " + data[i].destino;
				      }
			      }else{
			          if(data[i].fuente == ""){
			              data[i].operacion = "DEPOSITO";
				  }else{
				      data[i].operacion = "TRANSFERENCIA DE " + data[i].fuente;
				  }
			      }
                              subtotal += data[i].monto;
			      data[i].subtotal = subtotal;
                        }
			return data;
		})
	        .catch(err => alert(err));
    }

}

window.customElements.define('ui-movimientos', UIMovimientos);
