import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-card/paper-card.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';

class UIregister extends PolymerElement {

    static get template() {
        return html`
          <style>
            iron-pages{
              display:none !important;
            }

            .center {
              width:100%;
              text-align:center;
            }

            paper-card{
              width:37%;
              display:inline-block;
              margin-top: 3%;
            }
          </style>

          <div class="center">
            <br>
            <app-location route="{{route}}"></app-location>
            <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}"></app-route>
            <iron-ajax 
	      id="AjaxPost" 
              url="[[backendapp]]/cuentas/"
              method="POST"
              content-type="application/json"
              handle-as="json"
              on-response="handleResponse"
              on-error="handleAjaxPostError">
            </iron-ajax>

            <paper-card class="rate">
              <div class="card-content">
                <paper-input id="Nombre" name="nombre" float-label label="Nombre" required auto-validate pattern="[a-zA-Z \ñ\á\é\í\ó\ú\ç\ä\ë\ï\ö\ü]*" error-message="Solamente palabras" ></paper-input>
                <paper-input id="Dni" name="dni" float-label label="DNI" required auto-validate pattern="[0-9]*" error-message="Solamente números" char-counter maxlength="9"></paper-input>
                <paper-input id="Email" name="email" float-label label="e-mail" ></paper-input>
                <paper-input id="Clave" type="password" name="clave" float-label label="Clave" char-counter maxlength="10" ></paper-input>
                <paper-input id="ClaveR" type="password" name="claver" float-label label="Repetir clave" char-counter maxlength="10" ></paper-input>
              </div>

              <div class="card-actions">
                <paper-button raised on-tap="_register" class="indigo" style="background:green;color:#fff;width:100%">Registrar</paper-button>
              </div>
            </paper-card>
          </div>
        `;
    }

    constructor(){
        super();
    }

    ready(){
        super.ready();

        if (localStorage.getItem("isuser")=='True'){
           this.set('route.path', 'operar');
        };
    }

    static get properties () {
        return {
            _register: {
                type: Function
            },
            backendapp:{
                    type: String,
                    value: "http://backend:4001"
            }
        }
    }

    _register(){
        if(this.$.Clave.value == this.$.ClaveR.value){
	    var datos = { 
                "dni": this.$.Dni.value,
                "email": this.$.Email.value,
                "nombre": this.$.Nombre.value,
                "clave": this.$.Clave.value,
	    }
	    console.log(datos);
            this.$.AjaxPost.body = datos;
            this.$.AjaxPost.generateRequest();

	}else{
		alert("Las claves no coinciden");
	}
    }

    handleResponse(data){
        localStorage.setItem("isuser",'False');

        console.log(data.detail.__data.xhr.response);
        if (data.detail.__data.xhr.response.add=='OK'){
            this.$.Dni.value = "";
            this.$.Email.value = "";
            this.$.Nombre.value = "";
            this.$.Clave.value = "";
            this.$.ClaveR.value = "";
            // Ir a la pagina de login
            this.set('route.path', 'login');
        }else{
           alert(data.detail.__data.xhr.response.add);
        }            
    }

    handleAjaxPostError(event, request){
        alert("error en post");
    }

}

window.customElements.define('ui-register', UIregister);

