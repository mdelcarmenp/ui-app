/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-card/paper-card.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/iron-ajax/iron-ajax.js';

import './shared-styles.js';
import {UIApp} from './ui-app.js';

class UIOperar extends PolymerElement {

    constructor(){
       super();
    }

    ready(){
        super.ready();
        console.log("Operar");
        this.cuentaID=localStorage.getItem("cuentaID");
        console.log(this.cuentaID);
    }

    static get properties () {
        return {
            cuentaID: {
                type: String 
            },
            backendapp:{
                    type: String,
                    value: "http://backend:4001"
            },
	    visibleahora:{
		type: String,
		observer: '_visibleahoraChanged' 
	    },


        }
     }

     static get template() {
        return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>

      <ui-app>
            <iron-ajax 
	      id="AjaxPost" 
              url="[[backendapp]]/movimientos/agregar"
              method="POST"
              content-type="application/json"
              handle-as="json"
              on-response="handleResponse"
              on-error="handleAjaxPostError">
            </iron-ajax>


      <div class="card">
        <h1>Operar  Cuenta: {{cuentaID}}</h1>
        <p>Aquí podrá registar sus ingresos, retiros y transferencias de dinero</p>
        <p>INGRESAR - RETIRAR - TRANSFERIR</p>
	<paper-card heading="Ingresar" class="white" style="margin-bottom:8px;">
          <div class="card-content">
            <paper-input id="Concepto_ingresar" name="concepto_ingresar" float-label label="Concepto" char-counter maxlength="10"></paper-input>
            <paper-input id="Monto_ingresar" name="monto_ingresar" float-label label="Monto" type="Number"><div slot="prefix">$</div></paper-input>
	  </div>
	  <div class="card-acctions">
            <paper-button raised on-tap="_ingresar" class="indigo" style="background:blue;color:#fff;width:100%">Ingresar</paper-button>
          </div>
        </paper-card>
	<paper-card heading="Extraer" class="white" style="margin-bottom:8px;">
          <div class="card-content">
            <paper-input id="Concepto_extraer" name="concepto_extraer" float-label label="Concepto" char-counter maxlength="10"></paper-input>
            <paper-input id="Monto_extraer" name="monto_extraer" float-label label="Monto" type="Number"><div slot="prefix">$</div></paper-input>
	  </div>
	  <div class="card-acctions">
            <paper-button raised on-tap="_extraer" class="indigo" style="background:blue;color:#fff;width:100%">Extraer</paper-button>
          </div>
        </paper-card>
	<paper-card heading="Transferir" class="white" style="margin-bottom:8px;">
          <div class="card-content">
            <paper-input id="Concepto_transferir" name="concepto_transferir" float-label label="Concepto" char-counter maxlength="10"></paper-input>
            <paper-input id="Monto_transferir" name="monto_transferir" float-label label="Monto" type="number"><div slot="prefix">$</div></paper-input>
            <paper-input id="Destino" name="destino" float-label label="Cuenta"></paper-input>
	  </div>
	  <div class="card-acctions">
            <paper-button raised on-tap="_transferir" class="indigo" style="background:blue;color:#fff;width:100%">Transferir</paper-button>
          </div>
        </paper-card>
      </div>
      </ui-app>
    `;
  }
    _visibleahoraChanged(page){
	    if(page && page=="operar"){
		    console.log('Update data ' + page);
                    this.cuentaID=localStorage.getItem("cuentaID");
	    }
    }

    _ingresar(){
        console.log("Ingresar");
        this.$.AjaxPost.body = { 
		"accion": "ingresar",
		"monto": this.$.Monto_ingresar.value ,
		"concepto": this.$.Concepto_ingresar.value, 
		"cuentaID": this.cuentaID
	};
	this.$.Monto_ingresar.value = null;
	this.$.Concepto_ingresar.value ="";
        this.$.AjaxPost.generateRequest();
    }

    _extraer(){
        console.log("Extraer");
        this.$.AjaxPost.body = { 
		"accion": "extraer",
		"monto": this.$.Monto_extraer.value , 
		"concepto": this.$.Concepto_extraer.value, 
		"cuentaID": this.cuentaID
	};
	this.$.Monto_extraer.value = null;
	this.$.Concepto_extraer.value ="";
        this.$.AjaxPost.generateRequest();
    }

    _transferir(){
        console.log("Transferir");
        this.$.AjaxPost.body = { 
		"accion": "transferir",
		"monto": this.$.Monto_transferir.value ,
		"concepto": this.$.Concepto_transferir.value,
		"cuentaID": this.cuentaID,
		"destino": this.$.Destino.value
	};
	this.$.Monto_transferir.value = null;
	this.$.Concepto_transferir.value ="";
	this.$.Destino.value = "";
        this.$.AjaxPost.generateRequest();
    }

    handleResponse(data){
        console.log(data.detail.__data.xhr.response);
        if (data.detail.__data.xhr.response.isuser=='True' && data.detail.__data.xhr.response.movimiento=='OK'){
            //localStorage.setItem("cuentaID",data.detail.__data.xhr.response.cuentaID);
            localStorage.setItem("isuser",data.detail.__data.xhr.response.isuser);

            this.set('route.path', 'operar');
        }else{
           localStorage.setItem("isuser",data.detail.__data.xhr.response.isuser);
            alert("Error en la TX.");
        }            
    }

    handleAjaxPostError(event, request){
        alert("error");
    }

  
}

window.customElements.define('ui-operar', UIOperar);
