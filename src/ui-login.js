import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-card/paper-card.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';

class UIlogin extends PolymerElement {

    constructor(){
        super();
    }

    ready(){
        super.ready();

        if (localStorage.getItem("isuser")=='True'){
           this.set('route.path', 'operar');
           console.log("enter");
        };

    }

    static get properties () {
        return {
            _login: {
                type: Function
            },
	    backendapp:{
		    type: String,
		    value: "http://backend:4001"
	    }
        }
    }

    static get template() {
        return html`
          <style>
            iron-pages{
              display:none !important;
            }

            .center {
              width:100%;
              text-align:center;
            }

            paper-card{
              width:37%;
              display:inline-block;
              margin-top: 3%;
            }
          </style>

          <div class="center">
            <br>
            <app-location route="{{route}}"></app-location>
            <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}"></app-route>
            <iron-ajax 
	      id="AjaxPost" 
              url="[[backendapp]]/login"
              method="POST"
              content-type="application/json"
              handle-as="json"
              on-response="handleResponse"
              on-error="handleAjaxPostError">
            </iron-ajax>

            <paper-card class="rate">
              <div class="card-content">
                <paper-input id="Dni" name="dni" float-label label="DNI"  required auto-validate pattern="[0-9]*" error-message="Solamente números" char-counter maxlength="9"></paper-input>
                <paper-input id="Clave" type="password" name="clave" float-label label="Clave" ></paper-input>
              </div>

              <div class="card-actions">
                <paper-button raised on-tap="_login" class="indigo" style="background:blue;color:#fff;width:100%">Login</paper-button>
              </div>
              <div class="card-actions">
                <paper-button raised on-tap="register" class="indigo" style="background:green;color:#fff;width:100%">Registrarse</paper-button>
              </div>
            </paper-card>
          </div>
        `;
    }

    _login(){
        console.log("dni:"+this.$.Dni.value);
        console.log("passwd:"+this.$.Clave.value);
        this.$.AjaxPost.body = { "dni": this.$.Dni.value, "clave": this.$.Clave.value };
        this.$.AjaxPost.generateRequest();
    }

    register(){
	    this.set('route.path','register');
    }
    handleResponse(data){
        console.log(data.detail.__data.xhr.response);
        if (data.detail.__data.xhr.response.isuser=='True'){
            localStorage.setItem("cuentaID",data.detail.__data.xhr.response.cuentaID);
            localStorage.setItem("isuser",data.detail.__data.xhr.response.isuser);

	    //this.cuentaID = data.detail.__data.xhr.response.cuentaID;
            this.set('route.path', 'operar');
        }else{
           localStorage.setItem("isuser","False");
           localStorage.setItem("cuentaID","");
            alert("Dni o clave no válidos.");
        }            
    }

    handleAjaxPostError(event, request){
        alert("error");
    }

}

window.customElements.define('ui-login', UIlogin);

