/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-card/paper-card.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/paper-input/paper-input.js';

import './shared-styles.js';
//import {UIApp} from './ui-app.js';

class UIMisamigos extends PolymerElement {

  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>
      
      <ui-app>
      <div class="card">
        <h1>Mis amigos</h1>
        <p>PROXIMAMENTE aquí podrá ver sus amigos para poder transferirles su dinero</p>
        
      </div>
      </ui-app>
    `;
  }
    constructor(){
       super();
    }

    ready(){
        super.ready();
        console.log("MisAmigos");
        // XXX esto tiene que poder venir del host
        this.cuentaID=localStorage.getItem("cuentaID");
    }


    static get properties () {
        return {
	    cuentaID: {
		    type: String
	    },
	    misamigos:{
		type: Object
	    },
	    visibleahora:{
		type: String,
		observer: '_visibleahoraChanged' 
	    },
            backendapp:{
                    type: String,
                    value: "http://backend:4001"
            }


        }
    }
     _visibleahoraChanged(page){
	    if(page && page=="misdatos"){
		    console.log('Update data ' + page);
	            this._traerMisDatos(this.cuentaID);
	    }
     }
    _traerMisDatos(c){
	    console.log('la cuenta en mis amigos: ' + c);
	    console.log('la cuenta en this: '+ this.cuentaID);
	    fetch( this.backendapp+'/cuentas/'+ this.cuentaID + '/misamigos')
		.then(resp => resp.json())
	        .then(data => {
			console.log(data);
			this.misdatos = data;
		})
	        .catch(err => alert(err));
    }

    _modificar(e){
	 console.log('Modificar mis datos en cuenta ' + this.cuentaID);
         console.log(e.target.id);
	 var campo = e.target.id.split('_')[1];
	 if(campo != 'clave'){
             var obj = {};
	     obj[campo] = this.$['MisDatos_'+campo].value;
	 }else{
              if(this.$.MisDatos_clave.value != this.$.MisDatos_claveR.value){
		      alert('Las claves no coinciden');
		      return;
	      }else{
                      var obj = {'clave': this.$.MisDatos_clave.value};
	      }
	 }
	 console.log(obj);
	 var fetch_init = {
            method: 'POST', // or 'POST'
            headers: {
                  'Content-Type': 'application/json',
            },
            body: JSON.stringify(obj),
         }

	 fetch(this.backendapp+'/cuentas/'+ this.cuentaID, fetch_init)
	    .then((response) => response.json())
            .then((data) => {
                   console.log('Success:', data);
            })
            .catch((error) => {
                   console.error('Error:', error);
            });

    }
}

window.customElements.define('ui-misamigos', UIMisamigos);
