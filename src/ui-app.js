/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import './ui-icons.js';

// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
setRootPath(UIAppGlobals.rootPath);

class UIApp extends PolymerElement {

    constructor(){
        super();
    }

    ready(){
        super.ready();

        if (localStorage.getItem("isuser")=='True'){
            this.set('route.path', 'operar');
        }else{
            this.set('route.path', 'login');
        }
    }

    static get properties () {
        return {
            logout: {
                type: Function 
            },
            page: {
              type: String,
              reflectToAttribute: true
              //observer: '_pageChanged'
            },
            routeData: Object,
            subroute: Object

        }
    }

    logout(){
        localStorage.setItem("isuser","False");
        localStorage.setItem("cuentaID","");
        this.set('route.path', '/');
    }
    
  static get observers() {
    return [
      '_routePageChanged(routeData.page)'
    ];
  }

  _routePageChanged(page) {
     // Show the corresponding page according to the route.
     //
     // If no page was found in the route data, page will be an empty string.
     // Show 'login' in that case. And if the page doesn't exist, show 'view404'.
    if (!page) {
      this.page = 'login';
    } else if (['login','register','uiapp','operar', 'movimientos', 'misdatos','cambio','misamigos','ayuda'].indexOf(page) !== -1) {
      this.page = page;
    } else {
      this.page = 'view404';
    }

    // Close a non-persistent drawer when the page & route are changed.
     if (!this.$.drawer.persistent) {
        this.$.drawer.close();
     }
  }


    static get template() {
      return html`
      <style>
        :host {
          --app-primary-color: #4285f4;
          --app-secondary-color: black;

          display: block;
        }

        app-drawer-layout:not([narrow]) [drawer-toggle] {
          display: none;
        }

        app-header {
          color: #fff;
          background-color: var(--app-primary-color);
        }

        app-header paper-icon-button {
          --paper-icon-button-ink-color: white;
        }

        .drawer-list {
          margin: 0 20px;
        }

        .drawer-list a {
          display: block;
          padding: 0 16px;
          text-decoration: none;
          color: var(--app-secondary-color);
          line-height: 40px;
        }

        .drawer-list a.iron-selected {
          color: black;
          font-weight: bold;
        }
      </style>

      <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location>

      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>

      <app-drawer-layout fullbleed="" narrow="{{narrow}}">
        <!-- Drawer content -->
        <app-drawer id="drawer" slot="drawer" swipe-open="[[narrow]]">
          <app-toolbar>Menu</app-toolbar>
          <iron-selector selected="[[page]]" attr-for-selected="name" class="drawer-list" role="navigation">
            <a name="operar" href="[[rootPath]]operar">Operar</a>
            <a name="movimientos" href="[[rootPath]]movimientos">Movimientos</a>
            <a name="misdatos" href="[[rootPath]]misdatos">Mis datos</a>
            <a name="cambio" href="[[rootPath]]cambio">Cambio</a>
            <a name="misamigos" href="[[rootPath]]misamigos">Mis amigos</a>
            <a name="ayuda" href="[[rootPath]]ayuda">Ayuda</a>
          </iron-selector>
          
	  <paper-button raised on-tap="logout" class="indigo" style="background:red;color:#fff;width:80%">Salir</paper-button>

        </app-drawer>

        <!-- Main content -->
        <app-header-layout has-scrolling-region="">

          <app-header slot="header" condenses="" reveals="" effects="waterfall">
            <app-toolbar>
              <paper-icon-button icon="ui-icons:menu" drawer-toggle=""></paper-icon-button>
              <div main-title="">UI App</div>
            </app-toolbar>
          </app-header>

          <slot></slot>

        </app-header-layout>
      </app-drawer-layout>
    `;
    }

}

window.customElements.define('ui-app', UIApp);
