/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

// Navigation component

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
//import '@polymer/app-layout/app-drawer/app-drawer.js';
//import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
//import '@polymer/app-layout/app-header/app-header.js';
//import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import './ui-icons.js';

// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
setRootPath(UIAppGlobals.rootPath);

class UIAppNav extends PolymerElement {
  static get template() {
    return html`
      <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location>

      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>

          <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
            <ui-login name="login"></ui-login>
            <ui-register name="register"></ui-register>
            <ui-operar visibleahora="[[page]]" name="operar"></ui-operar>
            <ui-movimientos visibleahora="[[page]]" name="movimientos"></ui-movimientos>
            <ui-misdatos visibleahora="[[page]]" name="misdatos"></ui-misdatos>
            <ui-cambio visibleahora="[[page]]" name="cambio"></ui-cambio>
            <ui-misamigos visibleahora="[[page]]" name="misamigos"></ui-misamigos>
            <ui-ayuda visibleahora="[[page]]" name="ayuda"></ui-ayuda>
            <ui-view404 name="view404"></ui-view404>
          </iron-pages>
    `;
  }
    constructor(){
       super();
    }

    ready(){
        super.ready();
	import('./ui-operar.js');
	import('./ui-movimientos.js');
	import('./ui-misdatos.js');
	import('./ui-cambio.js');
	import('./ui-misamigos.js');
	import('./ui-ayuda.js');
    }

  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged',
	notify: true
      },
      routeData: Object,
      subroute: Object
    };
  }

  static get observers() {
    return [
      '_routePageChanged(routeData.page)'
    ];
  }

  _routePageChanged(page) {
     // Show the corresponding page according to the route.
     //
     // If no page was found in the route data, page will be an empty string.
     // Show 'login' in that case. And if the page doesn't exist, show 'view404'.
    if (!page) {
      this.page = 'login';
    } else if (['login','register','uiapp','operar', 'movimientos', 'misdatos','cambio','misamigos','ayuda'].indexOf(page) !== -1) {
      this.page = page;
    } else {
      this.page = 'view404';
    }

    // Close a non-persistent drawer when the page & route are changed.
   //  if (!this.$.drawer.persistent) {
    //    this.$.drawer.close();
    // }
  }

  _pageChanged(page) {
    // Import the page component on demand.
    //
    // Note: `polymer build` doesn't like string concatenation in the import
    // statement, so break it up.
    switch (page) {
      case 'uiapp':
        import('./ui-app.js');
        break;
      case 'login':
        import('./ui-login.js');
        break;
      case 'register':
        import('./ui-register.js');
        break;
      case 'operar':
        import('./ui-operar.js');
        break;
      case 'movimientos':
        import('./ui-movimientos.js');
        break;
      case 'misdatos':
        import('./ui-misdatos.js');
        break;
      case 'cambio':
        import('./ui-cambio.js');
        break;
      case 'misamigos':
        import('./ui-misamigos.js');
        break;
      case 'ayuda':
        import('./ui-ayuda.js');
        break;
      case 'view404':
        import('./ui-view404.js');
        break;
    }
  }
}

window.customElements.define('ui-appnav', UIAppNav);
