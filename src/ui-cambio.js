/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-card/paper-card.js';
import('./bwt-datatable-3.0.0/bwt-datatable.js');
import('./bwt-datatable-3.0.0/bwt-datatable-card.js');


import './shared-styles.js';
import {UIApp} from './ui-app.js';

class UICambio extends PolymerElement {
    constructor(){
       super();
    }

    ready(){
        super.ready();
        console.log("Cambio");
	// XXX esto tiene que poder venir del host
        this.cuentaID=localStorage.getItem("cuentaID");
        console.log(this.cuentaID);
    }

    static get properties () {
        return {
            cuentaID: {
                type: String 
            },
	    ticker: {
		type:  Array
	    },
	    visibleahora:{
		type: String,
		observer: '_visibleahoraChanged' 
	    }
        }
    }  
  

  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>

      <ui-app>
      <div class="card">
        <h1>Cotizacion de bitcoins</h1>
        <p>Última cotización según <a href="https://www.blockchain.com/prices">Blockchain.info</a></p>
	<paper-datatable data="[[ticker]]">
           <paper-datatable-column header="Moneda" property="moneda" ></paper-datatable-column>
           <paper-datatable-column header="Símbolo" property="symbol"></paper-datatable-column>
           <paper-datatable-column header="Última" property="last"></paper-datatable-column>
        </paper-datatable>
         
      </div>
      </ui-app>
    `;
  }

async     _visibleahoraChanged(page){
	    if(page && page=="cambio"){
		    console.log('Update data ' + page);
	            this.ticker = await this._traerTicker();
		    console.log(ticker);

	    }
    }

    
async   _traerTicker(){

            var url = 'https://blockchain.info';
	    var resp = await fetch(url+'/ticker')
              .then((response) => {
                  return response.json();
              })
              .then((myJson) => {
		    var monedas = ['USD', 'EUR','BRL'];
		    var arr = [];
		    monedas.forEach(moneda =>{
			    arr.push( {
				    "moneda": moneda,
				    "last": myJson[moneda].last,
				    "symbol": myJson[moneda].symbol
			    });
		    });
		    return arr;
              });
             return resp;
    }

}

window.customElements.define('ui-cambio', UICambio);
