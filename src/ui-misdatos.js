/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-card/paper-card.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/paper-input/paper-input.js';

import './shared-styles.js';
import {UIApp} from './ui-app.js';

class UIMisdatos extends PolymerElement {

  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>
      
      <ui-app>
      <div class="card">
        <h1>Modificar mis datos</h1>
        <p>Aquí podrá modificar sus datos</p>
	<paper-card heading="Nombre" class="horizontal" style="margin-bottom:8px;">
          <div class="card-content">
            <paper-input id="MisDatos_nombre" name="misdatos_nombre" value="{{misdatos.nombre}}" float-label label="Nombre" auto-validate pattern="[a-zA-Z \ñ\á\é\í\ó\ú\ç\ä\ë\ï\ö\ü]*" error-message="Solamente palabras"></paper-input>
	  </div>
	  <div class="card-actions">
            <paper-icon-button icon="check" id="misdatos_nombre_modificar" raised on-tap="_modificar" ></paper-icon-button>
            <paper-icon-button icon="cancel" id="misdatos_nombre_reset" raised on-tap="_reset" ></paper-icon-button>
          </div>
	</paper-card>

	<paper-card heading="E-Mail" class="rate" style="margin-bottom:8px;">
        <div class="card-content">
            <paper-input id="MisDatos_email" name="misdatos_email" value="{{misdatos.email}}" no-float-label></paper-input>
	  </div>
	  <div class="card-actions">
            <paper-icon-button icon="check" id="misdatos_email_modificar" raised on-tap="_modificar" ></paper-icon-button>
            <paper-icon-button icon="cancel" id="misdatos_email_reset" raised on-tap="_reset" ></paper-icon-button>
          </div>
	</paper-card>
	<paper-card heading="DNI" class="rate" style="margin-bottom:8px;">
        <div class="card-content">
            <paper-input id="MisDatos_dni" name="misdatos_dni" value="{{misdatos.dni}}" no-float-label auto-validate pattern="[0-9]*" error-message="Solamente números" char-counter maxlength="9"></paper-input>
	  </div>
	  <div class="card-actions">
            <paper-icon-button icon="check" id="misdatos_dni_modificar" raised on-tap="_modificar" ></paper-icon-button>
            <paper-icon-button icon="cancel" id="misdatos_dni_modificar" raised on-tap="_reset" ></paper-icon-button>
          </div>
	</paper-card>
	<paper-card heading="Clave" class="rate" style="margin-bottom:8px;">
          <div class="card-content">
            <paper-input type="password" id="MisDatos_clave" name="misdatos_clave" float-label label="Ingresar clave" char-counter maxlength="10"></paper-input>
            <paper-input type="password" id="MisDatos_claveR" name="misdatos_claver" value="{{misdatos.claver}}" float-label label="Repetir clave" char-counter maxlength="10"></paper-input>
	  </div>
	  <div class="card-actions">
            <paper-icon-button icon="check" id="misdatos_clave_modificar" raised on-tap="_modificar" ></paper-icon-button>
            <paper-icon-button icon="cancel" id="misdatos_clave_modificar" raised on-tap="_reset" ></paper-icon-button>
          </div>
	</paper-card>
        
	<paper-card heading="Baja del servicio" class="rate" style="margin-bottom:8px;">
          <div class="card-content">
            <paper-input id="MisDatos_baja" name="misdatos_baja" float-label label="Me quiero ir" required auto-validate pattern="Me quiero ir" error-message="Solo escribir la frase"></paper-input>
	  </div>
	  <div class="card-actions">
            <paper-button id="misdatos_baja_modificar" raised on-tap="_modificar" class="indigo" style="background:red;color:#fff;width:100%">Me quiero ir</paper-button>
          </div>
	</paper-card>
        
      </div>
      </ui-app>
    `;
  }
    constructor(){
       super();
    }

    ready(){
        super.ready();
        console.log("Misdatos");
        // XXX esto tiene que poder venir del host
        this.cuentaID=localStorage.getItem("cuentaID");
    }


    static get properties () {
        return {
	    cuentaID: {
		    type: String
	    },
	    misdatos:{
		type: Object
	    },
	    visibleahora:{
		type: String,
		observer: '_visibleahoraChanged' 
	    },
            backendapp:{
                    type: String,
                    value: "http://backend:4001"
            }


        }
    }
     _visibleahoraChanged(page){
	    if(page && page=="misdatos"){
		    console.log('Update data ' + page);
	            this._traerMisDatos(this.cuentaID);
	    }
     }
    _traerMisDatos(c){
	    console.log('la cuenta en mis datos: ' + c);
	    console.log('la cuenta en this: '+ this.cuentaID);
	    fetch(this.backendapp+'/cuentas/'+ this.cuentaID + '/misdatos')
		.then(resp => resp.json())
	        .then(data => {
			console.log(data);
			this.misdatos = data;
		})
	        .catch(err => alert(err));
    }

    _modificar(e){
	 console.log('Modificar mis datos en cuenta ' + this.cuentaID);
         console.log(e.target.id);
	 var campo = e.target.id.split('_')[1];
	 if(campo != 'clave'){
             var obj = {};
	     obj[campo] = this.$['MisDatos_'+campo].value;
	 }else if(campo == 'clave'){
              if(this.$.MisDatos_clave.value != this.$.MisDatos_claveR.value){
		      alert('Las claves no coinciden');
		      return;
	      }else{
                      var obj = {'clave': this.$.MisDatos_clave.value};
	      }
	 }else{
		 return;
	 }
	 console.log(obj);
	 var fetch_init = {
            method: 'POST', // or 'POST'
            headers: {
                  'Content-Type': 'application/json',
            },
            body: JSON.stringify(obj),
         }

	 fetch(this.backendapp+'/cuentas/'+ this.cuentaID, fetch_init)
	    .then((response) => response.json())
            .then((data) => {
                   console.log('Success:', data);
            })
            .catch((error) => {
                   console.error('Error:', error);
            });

    }
    _reset(){
		alert('Alcanza con cambiar de solapa');
    }
}

window.customElements.define('ui-misdatos', UIMisdatos);
