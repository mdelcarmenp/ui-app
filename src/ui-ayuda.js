/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-card/paper-card.js';

import './shared-styles.js';
import {UIApp} from './ui-app.js';

class UIAyuda extends PolymerElement {

  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>

      <ui-app>
      <div class="card">
        <h1>Cómo utilizar esta App</h1>
	<h2>Operar</h2>
	En este sector se podrán realizar ingreso, extracciones y transferencias en pesos (solo a cuentas existentes).
	Para cada tipo de operación ingresar una descripción de la operación (Concepto), la cantidad para operar (Monto) y en los casos que aplique, la cuenta a la que se quiere transferir.
	<h2>Movimientos</h2>
	Aquí se listan los movimientos de cuentas desde las más nuevas a las más antiguas. El monto total disponible se encuentra en la primera fila.
	<h2>Mis datos</h2>
	Aquí podrá modificar sus datos y dar de baja al servicio.
        <h2>Cambio</h2>
	Aquí podrá ver la cotización de bitcoins en algunas monedas comunes.
	<h2>Baja del servicio</h2>
        <p>Para poder dar de baja al servicio, en necesrio que la cuenta esté en 0</p>
      </div>
      </ui-app>
    `;
  }
}

window.customElements.define('ui-ayuda', UIAyuda);
